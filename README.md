# Motor Arena Documentation

The documentation of the final Game and project of [expert in video game development](http://cedv.uclm.es/). Motor Arena is a multiplayer game that is an adaptation of the motorcycle fights in the [Disney film Tron](https://en.wikipedia.org/wiki/Tron) using [Unreal Engine 4](https://www.unrealengine.com).

You can access the full Spanish documentation of Motor Arena: [MotorArenaDocumentation](https://www.dropbox.com/s/aymss80c8q9jgbx/MotorArenaDocumentacion.pdf?dl=0).

## Pictures

![](https://i.imgur.com/PC4KB45.png)

## License

Battleship is provided under [GNU General Public License Version 3](https://gitlab.com/CEDV19/CEDV_TFG_Documentation/blob/master/LICENSE).